PREFIX="qrbot"

run:
	@docker-compose -p $(PREFIX) up -d --force-recreate
