package com.og.qrbot.service;

import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.io.FileInputStream;
import java.io.IOException;

@Service
public class QrCodeService {
    public String readQRCode(String filePath, String charset)
            throws IOException, NotFoundException, ChecksumException, FormatException {
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(
                new BufferedImageLuminanceSource(
                        ImageIO.read(new FileInputStream(filePath)))));
        Result qrCodeResult = new QRCodeReader().decode(binaryBitmap);
        return qrCodeResult.getText();
    }
}
