package com.og.qrbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class QrbotApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(QrbotApplication.class, args);
    }

}
