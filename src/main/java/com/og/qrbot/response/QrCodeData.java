package com.og.qrbot.response;

public class QrCodeData {
    String qrCodeString;

    public String getQrCodeString() {
        return qrCodeString;
    }

    public void setQrCodeString(String qrCodeString) {
        this.qrCodeString = qrCodeString;
    }

}
