package com.og.qrbot.controller;

import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.og.qrbot.request.QrCodeDecodeRequest;
import com.og.qrbot.response.QrCodeData;
import com.og.qrbot.service.QrCodeService;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

@RestController
public class QrCodeDecodeController {

    private final QrCodeService qrCodeService;

    @Autowired
    public QrCodeDecodeController(QrCodeService qrCodeService) {
        this.qrCodeService = qrCodeService;
    }

    @PostMapping(value = "/decode")
    public QrCodeData decodeImage(@RequestBody QrCodeDecodeRequest qrCodeDecodeRequest) throws IOException, ChecksumException, NotFoundException, FormatException {
        String filepath = System.getProperty("java.io.tmpdir") + "/" + UUID.randomUUID();
        File file = new File(filepath);

        byte[] data = Base64.decodeBase64(qrCodeDecodeRequest.getImageBase64());
        try (OutputStream stream = new FileOutputStream(filepath)) {
            stream.write(data);
        } catch (IOException e) {
            // ?
        }

        QrCodeData qrCodeData = new QrCodeData();

        qrCodeData.setQrCodeString(this.qrCodeService.readQRCode(filepath, "UTF-8"));

        file.delete();

        return qrCodeData;
    }
}
